import config
import os
import re
import socket, struct
import stat
import storage
import subprocess
import uuid
import json
from random import randrange
from measurment_item import *
import bottle
import datetime


class Dummy(object):
    def __init__(self, data, text=""):
        self.data = data
        self.text = text

        for k, v in data.items():
            setattr(self, k, v)

    def __getitem__(self, k):
        return self.data[k]

    def __getattr__(self, k):
        return self.text

    def __str__(self):
        return str(self.data)


def check_program_is_installed(prg_name):
    return subprocess.call("which %s" % prg_name, shell=True) == 0


def current_tab(tab_name):
    setattr(config, "CURRENT_TAB", tab_name)


def is_tab_active(tabname):
    return 'active' if config.CURRENT_TAB == tabname else ''


class player():
    @staticmethod
    def is_installed():
        return check_program_is_installed('mpd') and check_program_is_installed('mpc')

    @staticmethod
    def play(song):
        _execute("mpc clear")
        _execute("mpc add %s" % song)
        _execute("mpc play 1")

    @staticmethod
    def stop():
        _execute("mpc clear")

    @staticmethod
    def volume(volume):
        _execute("mpc volume %s" % volume)


def execute_command(class_, action, extra_params):
    if config.COMMAND_EXECUTION == False:
        return "The command execution is NOT available."

    command = filter(lambda x: x['class_'] == class_ and
                               x['action'] == action,
                     storage.read('commands'))
    if not command:
        return "Command not found"

    command = command[0]

    for key, value in extra_params.items():
        command['command'] = command['command'].replace("$%s" % key, value)

    subprocess.call(command['command'], shell=True)
    return "Executing: %s" % command


def get_nodes_list(db):
    # Here will be SQL Statement
    db.execute("SELECT NodeId, Description ,Node_Address FROM  Nodes ")
    results = db.fetchall()

    node_dict = {}
    for row in results:
        id = row['NodeId']
        addr = row['Node_Address']
        descr = row['Description']
        node_dict[str(id)] = descr + " (" + addr + ")"

        # node_dict = {'1': "Node 1", '2':"Node 2",'3': 'Node 3'}

    return node_dict


def get_device_list(db, node_id, isJSON):
    if not node_id:
        return "Error: No device Id"

    db.execute('SELECT * FROM  devices_with_descriptions where Node_ID=%s', (node_id,))
    results = db.fetchall()

    device_dict = {}
    for row in results:
        id = row['device_ID']
        descr = row['device_Description']
        device_dict[str(id)] = descr

    if isJSON:
        return json.dumps(device_dict)
    else:
        return device_dict

def get_datatypes_list(db,device_id, isJSON):
    if not device_id:
        return "Error: No device Id"
    db.execute('SELECT idDatatypes_On_Devices, Data_type.Description as dype_desc, Data_resolution.Description as res_desc '
        'FROM  Datatypes_On_Devices '
        'INNER JOIN Data_type ON Datatypes_On_Devices.Data_type_id = Data_type.id '
        'INNER JOIN Data_resolution ON Datatypes_On_Devices.Data_resolution_id = Data_resolution.id '
        'WHERE  Devices_id =%s', (device_id,))
    results = db.fetchall()

    type_dict = {}
    for row in results:
        id      = row['idDatatypes_On_Devices']
        dataRes = row['res_desc']
        DataType = row['dype_desc']
        type_dict[str(id)] = DataType +' as '+ dataRes

    if isJSON:
        return json.dumps(type_dict)
    else:
        return device_dict



def get_historical_data(db, data):
    # data is expected to contain 'date_from', 'time_from', 'date_to', 'time_to', 'sensor_selection', 'node_selection'
    input_date_pattern = "%Y-%m-%dT%H:%M"
    sql_date_pattern = "%Y-%m-%d %H:%M:%S"

    try:
        dateTime_from = datetime.datetime.strptime(data["date_from"] + "T" + data["time_from"], input_date_pattern)
    except ValueError, e:
        raise bottle.HTTPError(400, "Invalid  date_from or time_from", e)

    try:
        dateTime_to = datetime.datetime.strptime(data["date_to"] + "T" + data["time_to"], input_date_pattern)
    except ValueError, e:
        raise bottle.HTTPError(400, "Invalid  date_to or time_to", e)

    try:
        device_id = int(data["sensor_selection"])
    except ValueError, e:
        raise bottle.HTTPError(400, "sensor_selection must be integer", e)

    try:
        node_id = int(data["node_selection"])
    except ValueError, e:
        raise bottle.HTTPError(400, "sensor_selection must be integer", e)


    try:
        datatype_id = int(data["datatype_selection"])
    except ValueError, e:
        raise bottle.HTTPError(400, "datatype_selection must be integer", e)



    #Make the SQL query
    # Datetime Data  device_id	NodeId
    #try:
    db.execute('SELECT  Datetime, Data FROM  historical_data_view where NodeId=%s AND device_id=%s '
               'AND Datetime <  %s AND Datetime > %s AND Datatypes_On_Devices_idDatatypes_On_Devices=%s ORDER BY Datetime ASC  ', (str(node_id), str(device_id), dateTime_to.strftime(sql_date_pattern),
                dateTime_from.strftime(sql_date_pattern),str(datatype_id) , ))
    results = db.fetchall()
    #except Exception, e:
    #     raise bottle.HTTPError(500, "Error executing Historical data sql statement.", e)

    measurment_item_list = []
    for row in results:
        val = row['Data']
        date_col = row['Datetime']
        one_item = measurment_item(date_col.strftime('%Y-%m-%dT%H:%M:%S'), float(val.strip()))
        measurment_item_list.append(one_item)

    #Fill measurement_item_list with returned measurements
    #in this stage we just  Generate random data
    '''
    dt_now = datetime.datetime.today() - datetime.timedelta(minutes=-1000)
    for x in range(1, 500):
        date = dt_now + datetime.timedelta(minutes=x)
        one_item = measurment_item(date.strftime('%Y-%m-%dT%H:%M:%S'), randrange(10))
        measurment_item_list.append(one_item)
    '''

    #create list of measurement dictionaries (because this can be converted to JSON)
    measurment_items_dictlist = []
    for item in measurment_item_list:
        measurment_items_dictlist.append(item.__dict__)

    return json.dumps(measurment_items_dictlist)


def _execute(cmd):
    try:
        output = subprocess.Popen(cmd.split(), stdout=subprocess.PIPE).communicate()
        return output[0]
    except OSError:
        return ""


# Yep, I like extremely long and descriptive names for
# functions and variables (if you haven't noticed it yet) =)
def execute_system_information_script():
    # Ok, let's explain this a little bit. The script system_info.sh gets information
    # about the system and throw the results to the STDOUT with a key-value format.
    # From here we execute the mentioned script and load it in Python Dictionary
    # dinamically, so, the names you use in the script for identifying a information
    # will be the same in the Python code. Please take a look to the comments of the
    # mentioned file for further information.
    result = _execute(os.getcwd() + "/scripts/system_info.sh")
    info = {}
    for line in result.split('\n'):
        try:
            constant, value = line.split(':', 1)
            info[constant] = value
        except ValueError:
            pass
    return info


class session():
    @staticmethod
    def create():
        session_id = uuid.uuid4().hex

        filepath = config.PATH_SESSION + session_id

        fd = os.open(filepath, os.O_CREAT, int("0400", 8))
        # XXX If I have a file descriptor without execute fdopen
        # is there something I need to close? :/
        f = os.fdopen(fd)
        f.close()

        return session_id

    @staticmethod
    def _check_permissions(file_path):
        try:
            st = os.stat(file_path)
        except OSError:
            return False
        filemode = stat.S_IMODE(st.st_mode)
        just_user_readable_permissions = filemode == int("0400", 8)
        same_username_that_executing_the_app = st.st_uid == os.getuid()
        return same_username_that_executing_the_app and just_user_readable_permissions

    @staticmethod
    def is_logged(session_id):
        if not session_id: return False

        file_path = config.PATH_SESSION + session_id
        return session._check_permissions(file_path)

    @staticmethod
    def logout(session_id):
        # The session id must have 32 chars - uuid4.
        # The session don't should have 'funny characters' because
        # we are using os.remove and an unauthenticated person could
        # potentially remove a file in the FS
        if not re.match('^[0-9a-f]{32}$', session_id):
            return
        try:
            os.remove(config.PATH_SESSION + session_id)
        except:
            pass


def in_whitelist(ips, check_ip):
    # I could use netaddr instead, but I don't want to add more
    # dependecies.
    # Receives a list of ips and a IP to be checked
    # Example: ips -> ["192.168.1.10", "192.168.1.11", "10.5.0.0/8"]
    # check_ip -> "10.5.1.2"
    # Returns  True
    def ip_to_int(ip):
        return struct.unpack('!L', socket.inet_aton(ip))[0]

    def mask_to_int(cidr_mask):
        # Creates a host-mask
        n = 32 - cidr_mask
        host_mask = (1L << n) - 1
        # Inverts it and we get a network-mask
        net_mask = (2 ** 32) - 1 - host_mask
        return net_mask

    def parse_ip(ip):
        ip, mask = ip.split('/') if '/' in ip else (ip, "32")
        ip = ip_to_int(ip)
        mask = mask_to_int(int(mask))
        return ip, mask

    def is_address_in_network(cidr_ip, check_ip):
        ip, mask = parse_ip(cidr_ip)
        network = ip & mask
        return (mask & ip_to_int(check_ip)) == network

    return any((is_address_in_network(ip, check_ip) for ip in ips ))


def is_need_to_update(json_payload):
    # Determines if default branch was pushed
    # If is then return True to indicate that new version
    # of source code should be downloded
    # Reference to payload that Bitbucket is sending:
    # https://confluence.atlassian.com/display/BITBUCKET/POST+hook+management

    if 'commits' not in json_payload:
        return False
    for commit in json_payload['commits']:
        branch_name = commit.get('branch')
        # To-do: do not hardcode branch name "default"
        if branch_name == "default":
            return True
    return False

