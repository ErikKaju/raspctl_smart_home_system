$( document).ready(function() {
    var liveUpdateTimerHandler=0;
    var chartData
    var chart
     $( ".datepicker" ).datepicker({
      showOn: "both",
      buttonImage: "http://demos.telerik.com/aspnet-ajax/webmail/Images/calendar.gif",
      buttonImageOnly: false,
      buttonText: "Select date",
      dateFormat: "yy-mm-dd"
    });

    $('.timepicker').timepicker({ 'scrollDefault': 'now',  'timeFormat': 'H:i'  });
    
    //callback handler for form submit
    $("#historical-stats-form").submit(function(e)
    {
        // Find disabled inputs, and remove the "disabled" attribute
        var disabled = $(this).find(':input:disabled').removeAttr('disabled');
        
        var postData = $(this).serializeArray();
        
        
        
        // re-disabled the set of inputs that you previously enabled
        disabled.attr('disabled','disabled');
        
        var formURL = $(this).attr("action");
        var jqxhr = $.ajax( {
            url : formURL,
            type: "POST",
            data : postData,
        })
        .done(function(result) {
            chartData=  $.parseJSON(result);
            chartData = parseDate(chartData);
            chart = loadAmChart(chartData);
            
            clearInterval(liveUpdateTimerHandler); //remove previous timer
            if ($("input#real_time").is(':checked')){
              liveUpdateTimerHandler =  startLiveChartUpdate(chart,5000);
            }
        })
            .fail(function() {
             alert( "error" );
        })
        .always(function() {
        
        });
        e.preventDefault(); //STOP default action
    });

    // parse dates
    function parseDate(chartData) {
      for( var i = 0; i < chartData.length; ++i )
        chartData[i]["date"] =  AmCharts.stringToDate(chartData[i]["date"], "YYYY-MM-DDTHH:NN:SS");
      return chartData;
    }
    //test change
    // Load Chart
    function loadAmChart(data) {
        var _chart = AmCharts.makeChart("chartdiv", {
            "type": "serial",
            "theme": "light",
            "pathToImages": "/static/js/amcharts/images/",
            "dataProvider": data,
            "valueAxes": [{
                "position": "left",
                "title": "Measurements"
            }],
            "graphs": [{
                "fillAlphas": 0.4,
                "valueField": "measurements"
            }],
            "chartScrollbar": {},
            "chartCursor": {
                "categoryBalloonDateFormat": "JJ:NN, DD MMMM",
                "cursorPosition": "mouse"
            },
            "categoryField": "date",
            "categoryAxis": {
                "minPeriod": "mm",
                "parseDates": true
            },
            "zoomOutOnDataUpdate":false
        });
    
        //chart.addListener("dataUpdated", zoomChart(_chart,data));
        // when we apply theme, the dataUpdated event is fired even before we add listener, so
        // we need to call zoomChart here
        zoomChart(_chart,data);
        // this method is called when chart is first inited as we listen for "dataUpdated" event
        return _chart;
    } 
        
        
    function zoomChart(chart,chartData) {
        // different zoom methods can be used - zoomToIndexes, zoomToDates, zoomToCategoryValues
        chart.zoomToIndexes(chartData.length - 250, chartData.length - 1);
    }
   

    function startLiveChartUpdate(chart, interval){
        var timerId = 0;
        timerId = setInterval(function () {

            /*
             * Post Ddata:
                date_from:2014-11-01
                time_from:00:00
                date_to:2014-12-01
                time_to:23:00
            */
            var last_element = chart.dataProvider[chart.dataProvider.length - 1];
            var last_hh = last_element["date"].getHours();
            var last_mm = last_element["date"].getMinutes();
            
            // Find disabled inputs, and remove the "disabled" attribute
            var disabled = $("#historical-stats-form").find(':input:disabled').removeAttr('disabled');
            
            var postData = $("#historical-stats-form").serializeArray();
            
            // re-disabled the set of inputs that you previously enabled
            disabled.attr('disabled','disabled');
            
            var newDate = new Date();
            var hh = newDate.getHours();
            var mm = newDate.getMinutes();
            
            for (var data in postData) {
                if (postData[data].name== "date_from") {
                    postData[data].value = $.datepicker.formatDate('yy-mm-dd', last_element["date"]);;
                }
                if (postData[data].name== "time_from") {
                    postData[data].value =last_hh + ":"  + last_mm;
                }
                if (postData[data].name== "date_to") {
                    postData[data].value =$.datepicker.formatDate('yy-mm-dd', newDate);
                }
                if (postData[data].name== "time_to") {
                    postData[data].value =hh +":"+ mm;
                }  
            }

            
            var formURL = $("#historical-stats-form").attr("action");
            var jqxhr = $.ajax( {
                url : formURL,
                type: "POST",
                data : postData,
            })
            .done(function(result) {
                var gotData=  $.parseJSON(result);
                gotData = parseDate(gotData);
                chart.dataProvider = chart.dataProvider.concat(gotData);
                chart.validateData();
            })
            
            
            /*
            
            // remove datapoint from the beginning
            chart.dataProvider.shift();
            
            // add new one at the end
            var newDate = new Date();
            var visits = Math.round(Math.random() * 40 + 10 + Math.random()  / 5);
            chart.dataProvider.push({
                date: newDate,
                measurements: measurements
            });
            
            chart.validateData();
            */
        }, interval);
        return timerId;
    }
   
    

    //Load List of devices/sensors after node is selected
    $("#node_selection").on('blur', function (e) {
        var valueSelected = this.value;
        var sel = $("#sensor_selection");
        sel.empty();
        sel.append('<option value="' + -1 + '">' + "Loading..." + '</option>');

        var jqxhr = $.ajax( {
            url : "JSON/historical_stats/nodes/" + this.value,
            type: "GET",
        })
        .done(function(data) {
            sel.empty();
            var items=  $.parseJSON(data);
            for (var key in items) {
              sel.append('<option value="' + key + '">' + items[key] + '</option>');
            }
        })
        .fail(function() {
             alert( "Error. In populating List of Devices" );
        })
    });
    
    //Load List of Datatypes and resolutions after node is selected
    $("#sensor_selection").on('blur', function (e) {
        var valueSelected = this.value;
        var sel = $("#datatype_selection");
        sel.empty();
        sel.append('<option value="' + -1 + '">' + "Loading..." + '</option>');
        
        var jqxhr = $.ajax( {
            url : "JSON/historical_stats/datatypes/" + this.value,
            type: "GET",
        })
        .done(function(data) {
            sel.empty();
            var items=  $.parseJSON(data);
            for (var key in items) {
                sel.append('<option value="' + key + '">' + items[key] + '</option>');
            }
        })
        .fail(function() {
            alert( "Error. In populating List of Devices" );
        })
    });
    
});

function disableToInputs() {
    document.getElementById("date_to").disabled = true;
    document.getElementById("time_to").disabled = true;
    $("#date_to").datepicker().datepicker('disable');
}

function enableToInputs() {
    document.getElementById("date_to").disabled = false;
    document.getElementById("time_to").disabled = false;
    $("#date_to").datepicker().datepicker('enable');
}

