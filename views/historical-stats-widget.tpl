<form class="historical-stats-form" name="historical-stats-form" id="historical-stats-form" method="post" action="/historical_stats/">

    <!-- Label and select list -->
    <label for="node_selection">Nodes </label>
    <select id="node_selection" name="node_selection" class="form-control">
      %for key, value in NodeList.iteritems():
      <option value="{{key}}">{{value}}</option>
      %end
    </select>

    <label for="sensor_selection">Device</label>
    <select id="sensor_selection" name="sensor_selection" class="form-control">
        %for key, value in DeviceList.iteritems():
        <option value="{{key}}">{{value}}</option>
        %end
    </select>

     <label for="datatype_selection">Data Type</label>
    <select id="datatype_selection" name="datatype_selection" class="form-control">
        %for key, value in DatatypeList.iteritems():
        <option value="{{key}}">{{value}}</option>
        %end
    </select>



  <fieldset class="historical-stats-fieldset">
      <input type="radio" name="real_time_or_history" id="real_time" value="radio 1" onclick="disableToInputs();" />
      <label for="real_time">Real-time</label>

      <input type="radio" name="real_time_or_history" id="history" value="radio 2" onclick="enableToInputs();"/>
      <label for="history">History</label>
  </fieldset>

  <hr />
  <fieldset class="historical-stats-fieldset">
    <legend>From:</legend>
    <input name="date_from" type="text" class="datepicker" value="{{date_from}}"/>
    <input name="time_from" type="text" class="timepicker" value="{{time_from}}"/>
  </fieldset>

  <hr />
  <fieldset class="historical-stats-fieldset">
    <legend>To:</legend>
    <input id="date_to" name="date_to" type="text" class="datepicker" value="{{date_to}}"/>
    <input id="time_to" name="time_to" type="text" class="timepicker" value="{{time_to}}"/>
  </fieldset>
  <hr />
  <input name="run" type="submit" value="Run!" class="btn-primary">
</form>

<div id="chartdiv"></div>

<!-- amCharts: http://www.amcharts.com/demos/area-with-time-based-data/  -->
<script type="text/javascript" src="/static/js/amcharts/amcharts.js"></script>
<script type="text/javascript" src="/static/js/amcharts/serial.js"></script>
<script type="text/javascript" src="/static/js/amcharts/themes/light.js"></script>

<!--Widget JavaScript -->
<script src="/static/js/historical-stats-widget.js"></script>