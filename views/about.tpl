% rebase base


<h3>About</h3>

<hr />
<h4>WEB service and Control&monitoring app for Raspberry PI based <a href="http://ati.ttu.ee/embsys/index.php/General_description_of_the_project" target="_blank">Smarthome</a> system</h4>
(c) Kalju Randjärv, Erik Kaju 2014</h4>
<hr/>
<p> In addition to standard RaspCTL possibilites to execute <a href="/commands">commands</a> in a easy way, see some <a href="/">statics</a> about what's going on on the RaspPi, there is a new <a href="/historical_stats">historical stats monitoring utility</a></p>


<hr />
<p>Solution is based on RaspCtl project by <a href="mailto:jcarreras@krenel.org">Jan Carreras Prat</a></p>

<br /> <br />

