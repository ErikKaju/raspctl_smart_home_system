% import helpers

<html style="overflow-y: scroll">
<head>
	<title> RaspCTL</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="/static/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link rel="stylesheet" href="/static/css/historical-stats-widget.css">

    <!-- Jquery UI library -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">

    <!-- TimePicker widget -->
    <link rel="stylesheet" href="/static/css/jquery.timepicker.css">

    <style>
    	body {
    		padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
      	}

		input[type="text"] {
			height: 26px;
		}
    </style>
</head>
<body>

   <div class="navbar navbar-fixed-top navbar-inverse" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/">RaspCTL</a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="{{ helpers.is_tab_active('commands')  }}"><a href="/commands"><span class="glyphicon"></span>Commands</a></li>
            <li class="{{ helpers.is_tab_active('services') }}"><a href="/services"><span class="glyphicon"></span>Services</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Utilities<span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li class="{{ helpers.is_tab_active('radio') }}"><a href="/radio"><span class="glyphicon"></span>Radio</a></li>
                <li class="{{ helpers.is_tab_active('alarm') }}"><a href="/alarm"><span class="glyphicon"></span>Alarm</a></li>
                <li class="{{ helpers.is_tab_active('historical-stats') }}"><a href="/historical_stats"><span class="glyphicon"></span>Historical stats</a></li>
                <li class="{{ helpers.is_tab_active('webcam') }}"><a href="/webcam"><span class="glyphicon"></span>Webcam</a></li>
              </ul>
            </li>
            <li class="{{ helpers.is_tab_active('config') }}"><a href="/config"><span class="glyphicon"></span>Configuration</a></li>
            <li class="{{ helpers.is_tab_active('about') }}"><a href="/about"><span class="glyphicon"></span>About</a></li>
          </ul>
          <ul class="nav navbar-nav">
            <li><a href="/logout"><span class="glyphicon"></span>Logout</a></li>
          </ul>
        </div><!-- /.nav-collapse -->
      </div><!-- /.container -->
    </div><!-- /.navbar -->

	<!-- OLD <script src="/static/js/jquery-1.9.0.min.js"></script>-->
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>

	<script src="/static/js/bootstrap.min.js"></script>

	<script src="/static/js/bootbox.min.js"></script>

    <!-- Jquery UI library -->
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

    <!-- TimePicker widget -->
    <script src="/static/js/jquery.timepicker.min.js"></script>


	<div class="container">
		%include
    	</div> <!-- /container -->

</body>
</html>
